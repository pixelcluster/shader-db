[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define USE_INSTANCE 
#define USE_GPU_SHADER_CREATE_INFO
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool1 a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define static
#  define inline
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#  define float2 vec2
#  define float3 vec3
#  define float4 vec4
#  define float4x4 mat4
#  define int2 ivec2
#  define int3 ivec3
#  define int4 ivec4
#  define uint2 uvec2
#  define uint3 uvec3
#  define uint4 uvec4
#  define bool1 bool
#  define bool2 bvec2
#  define bool3 bvec3
#  define bool4 bvec4

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  ifdef __cplusplus
#    include  BLI_float4x4.hh 
#    include  BLI_math_vec_types.hh 
using blender::float2;
using blender::float3;
using blender::float4;
using blender::float4x4;
using blender::int2;
using blender::int3;
using blender::int4;
using blender::uint2;
using blender::uint3;
using blender::uint4;
using bool1 = int;
using bool2 = blender::int2;
using bool3 = blender::int3;
using bool4 = blender::int4;

#  else /* C */
typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float float4x4[4][4];
typedef int int2[2];
typedef int int3[2];
typedef int int4[4];
typedef uint uint2[2];
typedef uint uint3[3];
typedef uint uint4[4];
typedef int bool1;
typedef int bool2[2];
typedef int bool3[2];
typedef int bool4[4];
#  endif

#endif
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 */

#ifndef USE_GPU_SHADER_CREATE_INFO
#  include  GPU_shader_shared_utils.h 
#endif

struct NodeLinkData {
  float4 colors[3];
  /* bezierPts Is actually a float2, but due to std140 each element needs to be aligned to 16
   * bytes. */
  float4 bezierPts[4];
  bool1 doArrow;
  bool1 doMuted;
  float dim_factor;
  float thickness;
  float dash_factor;
  float dash_alpha;
  float expandSize;
  float arrowSize;
};
BLI_STATIC_ASSERT_ALIGN(struct NodeLinkData, 16)

struct NodeLinkInstanceData {
  float4 colors[6];
  float expandSize;
  float arrowSize;
  float2 _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct NodeLinkInstanceData, 16)

struct GPencilStrokeData {
  float2 viewport;
  float pixsize;
  float objscale;
  float pixfactor;
  int xraymode;
  int caps_start;
  int caps_end;
  bool1 keep_size;
  bool1 fill_stroke;
  float2 _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct GPencilStrokeData, 16)

struct GPUClipPlanes {
  float4x4 ModelMatrix;
  float4 world[6];
};
BLI_STATIC_ASSERT_ALIGN(struct GPUClipPlanes, 16)

struct SimpleLightingData {
  float4 color;
  float3 light;
  float _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct SimpleLightingData, 16)

#define MAX_CALLS 16

struct MultiRectCallData {
  float4 calls_data[MAX_CALLS * 3];
};
BLI_STATIC_ASSERT_ALIGN(struct MultiRectCallData, 16)

/* Pass Resources. */
layout(binding = 0, std140) uniform node_link_data { NodeLinkInstanceData _node_link_data; };
#define node_link_data (_node_link_data)

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;


/* Inputs. */
layout(location = 0) in vec2 uv;
layout(location = 1) in vec2 pos;
layout(location = 2) in vec2 expand;
layout(location = 3) in vec2 P0;
layout(location = 4) in vec2 P1;
layout(location = 5) in vec2 P2;
layout(location = 6) in vec2 P3;
layout(location = 7) in ivec4 colid_doarrow;
layout(location = 8) in vec4 start_color;
layout(location = 9) in vec4 end_color;
layout(location = 10) in ivec2 domuted;
layout(location = 11) in float dim_factor;
layout(location = 12) in float thickness;
layout(location = 13) in float dash_factor;
layout(location = 14) in float dash_alpha;

/* Interfaces. */
out nodelink_iface{
  smooth vec4 finalColor;
  smooth float colorGradient;
  smooth float lineU;
  flat float lineLength;
  flat float dashFactor;
  flat float dashAlpha;
  flat int isMainLine;
};

/**
 * 2D Quadratic Bezier thick line drawing
 */

#define MID_VERTEX 65

/**
 * `uv.x` is position along the curve, defining the tangent space.
 * `uv.y` is "signed" distance (compressed to [0..1] range) from the pos in expand direction
 * `pos` is the verts position in the curve tangent space
 */

void main(void)
{
  /* Define where along the noodle the gradient will starts and ends.
   * Use 0.25 instead of 0.35-0.65, because of a visual shift issue. */
  const float start_gradient_threshold = 0.25;
  const float end_gradient_threshold = 0.55;

#ifdef USE_INSTANCE
#  define colStart (colid_doarrow[0] < 3 ? start_color : node_link_data.colors[colid_doarrow[0]])
#  define colEnd (colid_doarrow[1] < 3 ? end_color : node_link_data.colors[colid_doarrow[1]])
#  define colShadow node_link_data.colors[colid_doarrow[2]]
#  define doArrow (colid_doarrow[3] != 0)
#  define doMuted (domuted[0] != 0)
#else
  vec2 P0 = node_link_data.bezierPts[0].xy;
  vec2 P1 = node_link_data.bezierPts[1].xy;
  vec2 P2 = node_link_data.bezierPts[2].xy;
  vec2 P3 = node_link_data.bezierPts[3].xy;
  bool doArrow = node_link_data.doArrow;
  bool doMuted = node_link_data.doMuted;
  float dim_factor = node_link_data.dim_factor;
  float thickness = node_link_data.thickness;
  float dash_factor = node_link_data.dash_factor;
  float dash_alpha = node_link_data.dash_alpha;

  vec4 colShadow = node_link_data.colors[0];
  vec4 colStart = node_link_data.colors[1];
  vec4 colEnd = node_link_data.colors[2];
#endif

  /* Parameters for the dashed line. */
  isMainLine = expand.y != 1.0 ? 0 : 1;
  dashFactor = dash_factor;
  dashAlpha = dash_alpha;
  /* Approximate line length, no need for real bezier length calculation. */
  lineLength = distance(P0, P3);
  /* TODO: Incorrect U, this leads to non-uniform dash distribution. */
  lineU = uv.x;

  float t = uv.x;
  float t2 = t * t;
  float t2_3 = 3.0 * t2;
  float one_minus_t = 1.0 - t;
  float one_minus_t2 = one_minus_t * one_minus_t;
  float one_minus_t2_3 = 3.0 * one_minus_t2;

  vec2 point = (P0 * one_minus_t2 * one_minus_t + P1 * one_minus_t2_3 * t +
                P2 * t2_3 * one_minus_t + P3 * t2 * t);

  vec2 tangent = ((P1 - P0) * one_minus_t2_3 + (P2 - P1) * 6.0 * (t - t2) + (P3 - P2) * t2_3);

  /* tangent space at t */
  tangent = normalize(tangent);
  vec2 normal = tangent.yx * vec2(-1.0, 1.0);

  /* Position vertex on the curve tangent space */
  point += (pos.x * tangent + pos.y * normal) * node_link_data.arrowSize;

  gl_Position = ModelViewProjectionMatrix * vec4(point, 0.0, 1.0);

  vec2 exp_axis = expand.x * tangent + expand.y * normal;

  /* rotate & scale the expand axis */
  exp_axis = ModelViewProjectionMatrix[0].xy * exp_axis.xx +
             ModelViewProjectionMatrix[1].xy * exp_axis.yy;

  float expand_dist = (uv.y * 2.0 - 1.0);
  colorGradient = expand_dist;

  if (gl_VertexID < MID_VERTEX) {
    /* Shadow pass */
    finalColor = colShadow;
  }
  else {
    /* Second pass */
    if (uv.x < start_gradient_threshold) {
      finalColor = colStart;
    }
    else if (uv.x > end_gradient_threshold) {
      finalColor = colEnd;
    }
    else {
      /* Add 0.1 to avoid a visual shift issue. */
      finalColor = mix(colStart, colEnd, uv.x + 0.1);
    }
    expand_dist *= 0.5;
    if (doMuted) {
      finalColor[3] = 0.65;
    }
  }

  finalColor[3] *= dim_factor;

  /* Expand into a line */
  gl_Position.xy += exp_axis * node_link_data.expandSize * expand_dist * thickness;

  /* If the link is not muted or is not a reroute arrow the points are squashed to the center of
   * the line. Magic numbers are defined in drawnode.c */
  if ((expand.x == 1.0 && !doMuted) ||
      (expand.y != 1.0 && (pos.x < 0.70 || pos.x > 0.71) && !doArrow)) {
    gl_Position.xy *= 0.0;
  }
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define USE_INSTANCE 
#define USE_GPU_SHADER_CREATE_INFO
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool1 a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define static
#  define inline
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#  define float2 vec2
#  define float3 vec3
#  define float4 vec4
#  define float4x4 mat4
#  define int2 ivec2
#  define int3 ivec3
#  define int4 ivec4
#  define uint2 uvec2
#  define uint3 uvec3
#  define uint4 uvec4
#  define bool1 bool
#  define bool2 bvec2
#  define bool3 bvec3
#  define bool4 bvec4

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  ifdef __cplusplus
#    include  BLI_float4x4.hh 
#    include  BLI_math_vec_types.hh 
using blender::float2;
using blender::float3;
using blender::float4;
using blender::float4x4;
using blender::int2;
using blender::int3;
using blender::int4;
using blender::uint2;
using blender::uint3;
using blender::uint4;
using bool1 = int;
using bool2 = blender::int2;
using bool3 = blender::int3;
using bool4 = blender::int4;

#  else /* C */
typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float float4x4[4][4];
typedef int int2[2];
typedef int int3[2];
typedef int int4[4];
typedef uint uint2[2];
typedef uint uint3[3];
typedef uint uint4[4];
typedef int bool1;
typedef int bool2[2];
typedef int bool3[2];
typedef int bool4[4];
#  endif

#endif
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 */

#ifndef USE_GPU_SHADER_CREATE_INFO
#  include  GPU_shader_shared_utils.h 
#endif

struct NodeLinkData {
  float4 colors[3];
  /* bezierPts Is actually a float2, but due to std140 each element needs to be aligned to 16
   * bytes. */
  float4 bezierPts[4];
  bool1 doArrow;
  bool1 doMuted;
  float dim_factor;
  float thickness;
  float dash_factor;
  float dash_alpha;
  float expandSize;
  float arrowSize;
};
BLI_STATIC_ASSERT_ALIGN(struct NodeLinkData, 16)

struct NodeLinkInstanceData {
  float4 colors[6];
  float expandSize;
  float arrowSize;
  float2 _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct NodeLinkInstanceData, 16)

struct GPencilStrokeData {
  float2 viewport;
  float pixsize;
  float objscale;
  float pixfactor;
  int xraymode;
  int caps_start;
  int caps_end;
  bool1 keep_size;
  bool1 fill_stroke;
  float2 _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct GPencilStrokeData, 16)

struct GPUClipPlanes {
  float4x4 ModelMatrix;
  float4 world[6];
};
BLI_STATIC_ASSERT_ALIGN(struct GPUClipPlanes, 16)

struct SimpleLightingData {
  float4 color;
  float3 light;
  float _pad;
};
BLI_STATIC_ASSERT_ALIGN(struct SimpleLightingData, 16)

#define MAX_CALLS 16

struct MultiRectCallData {
  float4 calls_data[MAX_CALLS * 3];
};
BLI_STATIC_ASSERT_ALIGN(struct MultiRectCallData, 16)

/* Pass Resources. */
layout(binding = 0, std140) uniform node_link_data { NodeLinkInstanceData _node_link_data; };
#define node_link_data (_node_link_data)

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;


/* Interfaces. */
in nodelink_iface{
  smooth vec4 finalColor;
  smooth float colorGradient;
  smooth float lineU;
  flat float lineLength;
  flat float dashFactor;
  flat float dashAlpha;
  flat int isMainLine;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


#define DASH_WIDTH 10.0
#define ANTIALIAS 1.0
#define MINIMUM_ALPHA 0.5

void main()
{
  fragColor = finalColor;

  if ((isMainLine != 0) && (dashFactor < 1.0)) {
    float distance_along_line = lineLength * lineU;
    float normalized_distance = fract(distance_along_line / DASH_WIDTH);

    /* Checking if `normalized_distance <= dashFactor` is already enough for a basic
     * dash, however we want to handle a nice antialias. */

    float dash_center = DASH_WIDTH * dashFactor * 0.5;
    float normalized_distance_triangle =
        1.0 - abs((fract((distance_along_line - dash_center) / DASH_WIDTH)) * 2.0 - 1.0);
    float t = ANTIALIAS / DASH_WIDTH;
    float slope = 1.0 / (2.0 * t);

    float unclamped_alpha = 1.0 - slope * (normalized_distance_triangle - dashFactor + t);
    float alpha = max(dashAlpha, min(unclamped_alpha, 1.0));

    fragColor.a *= alpha;
  }

  fragColor.a *= smoothstep(1.0, 0.1, abs(colorGradient));
}

